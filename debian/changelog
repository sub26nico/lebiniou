lebiniou (3.66.0-1) unstable; urgency=medium

  * New upstream release 3.66.0.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 06 Mar 2022 19:20:35 +0100

lebiniou (3.65.0-1) unstable; urgency=medium

  * New upstream release 3.65.0.
  * src/Makefile.am: Fix dh_shlibdeps issues.
  * debian/copyright: Update copyright years.
  * plugins/main/video/video.c: Fix FTBFS with ffmpeg 5.0 (Closes: #1004722)

 -- Olivier Girondel <olivier@biniou.info>  Sat, 22 Jan 2022 17:39:37 +0100

lebiniou (3.64.0-1) unstable; urgency=medium

  * New upstream release 3.64.0.
  * debian/control: Depends: Removed libjs-jquery, libjs-jquery-ui.
  * debian/control: Depends, Breaks, Replaces: Bump lebiniou-data to 3.63.0.
  * debian/test/control: Depends: lebiniou-data (>= 3.64.0)
  * debian/test/control: Depends: Removed lebiniou.


 -- Olivier Girondel <olivier@biniou.info>  Fri, 31 Dec 2021 15:16:45 +0100

lebiniou (3.63.4-1) unstable; urgency=medium

  * New upstream release 3.63.4.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 27 Nov 2021 19:47:34 +0100

lebiniou (3.63.3-1) unstable; urgency=medium

  * New upstream release 3.63.3.
  * plugins/output/mp4/mp4.c: Removed a debug message breaking autopkgtest.

 -- Olivier Girondel <olivier@biniou.info>  Fri, 12 Nov 2021 12:04:55 +0100

lebiniou (3.63.2-1) unstable; urgency=medium

  * New upstream release 3.63.2.
  * debian/copyright: Updated. (Closes: #998617)
  * plugins/output/caca/caca.c: Prevent a FTBFS with libcaca 0.99.beta20.
  * plugins/input/sndio/sndio.c: Fix a FTBFS on i386.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 11 Nov 2021 16:50:44 +0100

lebiniou (3.62.1-3) unstable; urgency=medium

  * debian/test/control: Update Depends: lebiniou-data (>= 3.62.2).

 -- Olivier Girondel <olivier@biniou.info>  Tue, 19 Oct 2021 10:57:58 +0200

lebiniou (3.62.1-2) unstable; urgency=medium

  * debian/control: Add Replaces: lebiniou-data (<< 3.61.0) (Closes: #995644).

 -- Olivier Girondel <olivier@biniou.info>  Sat, 16 Oct 2021 16:18:55 +0200

lebiniou (3.62.1-1) unstable; urgency=medium

  * New upstream release 3.62.1.
  * debian/changelog: Bump urgency to medium.
  * debian/tests/control: Remove wget.

 -- Olivier Girondel <olivier@biniou.info>  Tue, 21 Sep 2021 21:46:27 +0200

lebiniou (3.62.0-1) unstable; urgency=low

  * New upstream release 3.62.0.
  * debian/control: Build-Depends: Remove htmlmin, python3-setuptools.
  * debian/control: Build-Depends: Add liblo-dev.
  * debian/copyright: Update.
  * debian/lebiniou.lintian-overrides: Update.
  * debian/rules: Remove override_dh_autoconfigure.
  * debian/source/lintian-overrides: Remove.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 12 Sep 2021 00:07:32 +0200

lebiniou (3.61.2-1) unstable; urgency=low

  * New upstream release 3.61.2.
  * Fix FTBFS on hurd-i386.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 28 Aug 2021 16:04:25 +0200

lebiniou (3.61.1-2) unstable; urgency=low

  * debian/control: Add Breaks: lebiniou-data (<< 3.61.0) (Closes: #992919).

 -- Olivier Girondel <olivier@biniou.info>  Fri, 27 Aug 2021 12:29:37 +0200

lebiniou (3.61.1-1) unstable; urgency=low

  * New upstream release 3.61.1.
  * debian/control: Build-Depends: Remove libfreetype6-dev.
  * Fix FTBFS on armel/armhf/i386.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 23 Aug 2021 14:55:43 +0200

lebiniou (3.61.0-1) unstable; urgency=low

  * New upstream release 3.61.0.
  * debian/control: Update Depends: lebiniou-data (>= 3.61.0).
  * debian/control: Build-Depends: Add htmlmin, python3-setuptools.
  * debian/control: Build-Depends: Remove libsdl2-ttf-dev.
  * debian/control: Build-Depends: Add libsdl2-dev.
  * debian/control: Depends: Remove fonts-freefont-ttf.
  * debian/control: Standards-Version: 4.6.0.0.
  * debian/copyright: Updated.
  * debian/lebiniou.lintian-overrides: Updated.
  * debian/source/lintian-overrides: Updated.
  * debian/tests/control: Depends: Add wget, bump lebiniou-data version.
  * debian/tests/control: Add Restrictions: needs-internet.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 18 Aug 2021 16:46:01 +0200

lebiniou (3.54.1-1) unstable; urgency=low

  * New upstream release 3.54.1.
  * debian/control: Build-Depends: Remove uglifyjs, cleancss.
  * debian/rules: Add override_dh_auto_configure.

 -- Olivier Girondel <olivier@biniou.info>  Tue, 16 Feb 2021 13:30:25 +0100

lebiniou (3.54.0-1) unstable; urgency=low

  * New upstream release 3.54.0.
  * debian/control: Depends: Update lebiniou-data (>= 3.54.1).
  * debian/control: Build-Depends: Add uglifyjs, cleancss.
  * debian/lebiniou.lintian-overrides: Updated.
  * debian/source/lintian-overrides: Updated.
  * debian/lebiniou.examples: Removed.
  * debian/tests/control: Bump lebiniou-data minimal version.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 14 Feb 2021 15:46:45 +0100

lebiniou (3.53.3-1) unstable; urgency=low

  * New upstream release 3.53.3.
  * debian/tests/control: Add lebiniou.

 -- Olivier Girondel <olivier@biniou.info>  Fri, 29 Jan 2021 13:26:18 +0100

lebiniou (3.53.2-2) unstable; urgency=low

  * debian/tests/control: Fix typo.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 28 Jan 2021 22:02:27 +0100

lebiniou (3.53.2-1) unstable; urgency=low

  * New upstream release 3.53.2.
  * debian/tests/control: Update lebiniou-data minimal version.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 20 Jan 2021 20:56:29 +0100

lebiniou (3.53.1-1) unstable; urgency=low

  * New upstream release 3.53.1.
  * configure.ac, src/Makefile.am: Fix FTBFS on alpha, ppc64.
  * debian/tests/control: Update lebiniou-data minimal version.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 20 Jan 2021 14:45:39 +0100

lebiniou (3.53.0-1) unstable; urgency=low

  * New upstream release 3.53.0.
  * debian/control: Update description.
  * debian/copyright: Update.
  * debian/tests/control: Add a test script to remove Restrictions: superficial.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 17 Jan 2021 17:00:31 +0100

lebiniou (3.52.0-1) unstable; urgency=low

  * New upstream release 3.52.0.
  * debian/copyright: Updated.
  * debian/lebiniou.links: Updated.
  * debian/lebiniou.lintian-overrides: Updated.
  * debian/source/lintian-overrides: Updated.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 03 Jan 2021 15:01:20 +0100

lebiniou (3.51-2) unstable; urgency=low

  * debian/control: Standards-Version: 4.5.1.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 07 Dec 2020 18:07:46 +0100

lebiniou (3.51-1) unstable; urgency=low

  * New upstream release 3.51.
  * debian/control: Build-Depends: Remove pandoc.
  * debian/control: Build-Depends: Add libavcodec-dev, libavformat-dev.
  * debian/lebiniou.doc-base: Removed.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 07 Dec 2020 16:05:37 +0100

lebiniou (3.50-1) unstable; urgency=low

  * New upstream release 3.50.
  * debian/control: Depends: Add libjs-jquery, libjs-jquery-ui.
  * debian/control: Depends: Add lebiniou-data (>= 3.50).
  * debian/control: Build-Depends: Remove libxml2-dev (>= 2.6).
  * debian/copyright: Updated.
  * debian/lebiniou.links: Added.
  * debian/lebiniou.lintian-overrides: Updated.
  * debian/source/lintian-overrides: Added.

 -- Olivier Girondel <olivier@biniou.info>  Fri, 18 Sep 2020 15:49:30 +0200

lebiniou (3.43.1-2) unstable; urgency=low

  * debian/tests/control: Add Restrictions: superficial (Closes: #969837).

 -- Olivier Girondel <olivier@biniou.info>  Fri, 18 Sep 2020 14:25:49 +0200

lebiniou (3.43.1-1) unstable; urgency=low

  * Fix FTBFS on armel/armhf/i386/mipsel.
  * New upstream release 3.43.1.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 15 Aug 2020 20:37:02 +0200

lebiniou (3.43-1) unstable; urgency=low

  * New upstream release 3.43.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 13 Jul 2020 14:25:58 +0200

lebiniou (3.42.1-2) unstable; urgency=low

  * debian/control: Update Depends: lebiniou-data (>= 3.42).

 -- Olivier Girondel <olivier@biniou.info>  Wed, 13 May 2020 14:47:21 +0200

lebiniou (3.42.1-1) unstable; urgency=low

  * New upstream release 3.42.1.

 -- Olivier Girondel <olivier@biniou.info>  Tue, 12 May 2020 14:58:29 +0200

lebiniou (3.42-1) unstable; urgency=low

  * New upstream release 3.42.
  * debian/control: Build-Depends: Remove libulfius-dev.
  * debian/control: Build-Depends: Use debhelper-compat (= 13).

 -- Olivier Girondel <olivier@biniou.info>  Thu, 30 Apr 2020 16:56:36 +0200

lebiniou (3.41-1) unstable; urgency=low

  * New upstream release 3.41.
  * Fix FTBFS with gcc-10 (Closes: #957421)

 -- Olivier Girondel <olivier@biniou.info>  Fri, 17 Apr 2020 15:56:37 +0200

lebiniou (3.40-1) unstable; urgency=low

  * New upstream release 3.40.
  * debian/control: Add Build-Depends: libjansson-dev, libulfius-dev
  * debian/tests/control: Fix Test-Command. (Closes: #947367)
  * debian/lebiniou.lintian-overrides: Commented.
  * debian/copyright: Update copyright years.
  * debian/control: Standards-Version: 4.5.0.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 07 Dec 2019 21:25:22 +0100

lebiniou (3.32-1) unstable; urgency=low

  * New upstream release 3.32.
  * debian/control: Standards-Version: 4.4.1.
  * debian/lebiniou.lintian-overrides: Added.
  * debian/control: Build-Depends: Update debhelper-compat (= 12).
  * debian/control: Add Rules-Requires-Root: no.
  * debian/compat: Remove.
  * debian.rules: Add override_dh_shlibdeps.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 09 Oct 2019 17:00:55 +0200

lebiniou (3.31-1) unstable; urgency=low

  * New upstream release 3.31.
  * debian/control: Remove libpnglite-dev.
  * debian/control: Add libmagickwand-dev.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 23 Jan 2019 10:36:47 +0100

lebiniou (3.30-1) unstable; urgency=low

  * New upstream release 3.30.
  * debian/control: Standards-Version: 4.3.0.
  * debian/copyright: Update copyright years.

 -- Olivier Girondel <olivier@biniou.info>  Fri, 11 Jan 2019 14:54:28 +0100

lebiniou (3.29-1) unstable; urgency=low

  * New upstream release 3.29.
  * debian/control: Remove Build-Depends: libsdl-ttf2.0-dev.
  * debian/control: Add Build-Depends: libsdl2-ttf-dev, pandoc, libsndfile1-dev.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 28 Nov 2018 17:25:53 +0100

lebiniou (3.28-1) unstable; urgency=low

  * New upstream release 3.28.
  * configure.ac: armhf fixes.
  * configure.ac: Possible fix for a FTBFS on ia64.

 -- Olivier Girondel <olivier@biniou.info>  Tue, 13 Nov 2018 13:12:08 +0100

lebiniou (3.27-1) unstable; urgency=low

  * New upstream release 3.27.
  * debian/control: Remove erroneous Build-Depends on dh-make.
  * debian/control: Depends: lebiniou-data (>= 3.27).

 -- Olivier Girondel <olivier@biniou.info>  Sat, 27 Oct 2018 18:26:38 +0200

lebiniou (3.26-1) unstable; urgency=low

  * New upstream release 3.26.
  * debian/compat: Was actually 11.
  * debian/control: Update debhelper version.
  * debian/control: Vcs-Browser and Vcs-Git were missing.
  * debian/control: Add Build-Depends: libjack-dev.
  * configure.ac: Possible fix for a FTBFS on hurd-i386.
  * configure.ac: *BSD fixes.
  * configure.ac: Fix FTBFS with clang.
  * Makefile.am: Add README.md to the archive.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 15 Oct 2018 12:48:11 +0200

lebiniou (3.25-1) unstable; urgency=low

  * New upstream release 3.25.
  * debian/compat: Upgrade to 10.
  * debian/control: Standards-Version: 4.2.1.
  * debian/control: Update Homepage.
  * debian/control: Priority: optional.
  * debian/control: Vcs-Browser: https://gitlab.com/lebiniou/lebiniou.
  * debian/control: Vcs-Git: https://gitlab.com/lebiniou/lebiniou.git.
  * debian/control: Build-Depends: Remove autotools-dev.
  * debian/control: Testsuite: autopkgtest.
  * debian/copyright: Update copyright years.
  * debian/copyright: Update Source URL.
  * debian/copyright: Use secure copyright format URI.
  * debian/rules: Remove --parallel.
  * debian/rules: Remove --with autotools_dev.
  * debian/tests/control: Added.
  * debian/watch: version=4.
  * debian/watch: opts="pgpmode=none".
  * debian/watch: Use secure URI.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 01 Oct 2018 13:30:04 +0200

lebiniou (3.24-1) unstable; urgency=low

  * New upstream release 3.24
  * debian/control: Standards-Version: 3.9.8. (Closes: #827991)
  * src/Makefile.am: Fix lintian warning: hardening-no-pie.
  * configure.ac: Remove unnecessary linking with zlib.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 23 Jun 2016 18:01:05 +0200

lebiniou (3.23-1) unstable; urgency=low

  * New upstream release 3.23
  * Fix FTBFS against recent ffmpeg. (Closes: #803830)
  * debian/control: Standards-Version: 3.9.6.
  * debian/copyright: Update copyright years.
  * debian/menu: Removed.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 03 Mar 2016 22:03:27 +0200

lebiniou (3.22-1) unstable; urgency=low

  * New upstream release 3.22.
  * Fix some bugs found with clang.
  * Fix FTBFS against libav11. (Closes: #758205)

 -- Olivier Girondel <olivier@biniou.info>  Fri, 15 Aug 2014 16:04:28 +0200

lebiniou (3.21-1) unstable; urgency=low

  * New upstream release 3.21.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 16 Jun 2014 15:21:30 +0200

lebiniou (3.20-1) unstable; urgency=low

  * New upstream release 3.20.
  * debian/control: Depend on fonts-freefont-ttf. (Closes: #738230)
  * debian/copyright: Update copyright years.
  * debian/control: Standards-Version: 3.9.5.

 -- Olivier Girondel <olivier@biniou.info>  Wed, 12 Feb 2014 02:53:12 +0200

lebiniou (3.19.1-1) unstable; urgency=low

  * configure.ac: Fix FTBFS on kbsd32, kbsd64. (Closes: #720656)
  * configure.ac: LFS support.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 24 Aug 2013 13:55:32 +0200

lebiniou (3.19-1) unstable; urgency=low

  * New upstream release 3.19.
  * debian/control: Add missing Build-Depends: libavutil-dev. (Closes: #713513)
  * debian/control: Standards-Version: 3.9.4.
  * debian/compat: Upgrade to 9.
  * debian/control: Build-Depends: debhelper (>= 9).
  * debian/copyright: Update copyright years.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 22 Aug 2013 20:44:32 +0200

lebiniou (3.18-1) unstable; urgency=high

  * New upstream release 3.18.
  * Support older libswscale.
  * Add missing Build-Depends: libfreetype6-dev, libasound2-dev,
    libpulse-dev. (Closes: #669437)

 -- Olivier Girondel <olivier@biniou.info>  Sun, 22 Apr 2012 22:07:40 +0200

lebiniou (3.17-1) unstable; urgency=low

  * New upstream release 3.17.
  * Fix "Bug running two lebiniou with different webcams." (Closes: #662722)

 -- Olivier Girondel <olivier@biniou.info>  Fri, 06 Apr 2012 16:52:06 +0200

lebiniou (3.16.1-1) unstable; urgency=low

  * New upstream release 3.16.1.
  * Fix FTBFS with gcc-4.7. (Closes: #667237)

 -- Olivier Girondel <olivier@biniou.info>  Wed, 04 Apr 2012 00:12:36 +0200

lebiniou (3.16-1) unstable; urgency=low

  * New upstream release 3.16.
  * Added JACK Audio support. (Closes: #660921)
  * Support up to 2 webcams.
  * Fix FTBFS on hurd-i386.

 -- Olivier Girondel <olivier@biniou.info>  Thu, 29 Mar 2012 19:25:25 +0200

lebiniou (3.15-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "lebiniou FTBFS on architectures where char is unsigned."
    - Thanks Peter Green (Closes: #660947)

 -- Hector Oron <zumbi@debian.org>  Mon, 05 Mar 2012 01:07:16 +0000

lebiniou (3.15-1) unstable; urgency=low

  * New upstream release 3.15.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 21 Jan 2012 23:58:21 +0200

lebiniou (3.14-1) unstable; urgency=low

  * New upstream release 3.14.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 07 Nov 2011 22:30:54 +0200

lebiniou (3.13-1) unstable; urgency=low

  * New upstream release 3.13.
  * Put all library flags in LIBS instead of LDFLAGS. (Closes: #647100)

 -- Olivier Girondel <olivier@biniou.info>  Mon, 22 Oct 2011 21:04:28 +0200

lebiniou (3.12-1) unstable; urgency=low

  * New upstream release 3.12.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 03 Oct 2011 20:27:28 +0200

lebiniou (3.11-1) unstable; urgency=low

  * New upstream release 3.11.

 -- Olivier Girondel <olivier@biniou.info>  Sat, 24 Sep 2011 23:37:13 +0200

lebiniou (3.10-1) unstable; urgency=low

  * New upstream release 3.10.

 -- Olivier Girondel <olivier@biniou.info>  Sun, 11 Sep 2011 02:40:28 +0200

lebiniou (3.9.2-1) unstable; urgency=low

  * New upstream release 3.9.2.
  * Fix FTBFS on ia64. (Closes: #637696)
  * Fix FTBFS on mips/mipsel. (Closes: #637699)

 -- Olivier Girondel <olivier@biniou.info>  Sat, 13 Aug 2011 20:46:12 +0200

lebiniou (3.9.1-1) unstable; urgency=low

  * New upstream release 3.9.1.
  * fonts/FreeMono.ttf was removed from upstream sources.

 -- Olivier Girondel <olivier@biniou.info>  Mon, 08 Aug 2011 17:00:00 +0200

lebiniou (3.9-1) unstable; urgency=low

  * Initial Release. (Closes: #610042)

 -- Olivier Girondel <olivier@biniou.info>  Wed, 27 Apr 2011 22:00:00 +0200
