/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gstdio.h>
#include "globals.h"
#include "sequences.h"


void
Sequences_remove_sequence_by_name(const char *name)
{
  GList *sequence = Sequences_find_by_name(name);

  if (NULL != sequence) {
    Sequence_t *seq = (Sequence_t *)sequence->data;

    sequences->seqs = g_list_remove(sequences->seqs, seq);
    sequences->size = g_list_length(sequences->seqs);
    Sequence_delete(seq);
    Shuffler_disable(sequences->shuffler, sequences->size);
  }
}
