/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_ULFIUS_H
#define __BINIOU_ULFIUS_H

#include <ulfius.h>
#include "context.h"
#include "commands.h"

#define BULFIUS_GET     "GET"
#define BULFIUS_POST    "POST"
#define BULFIUS_OPTIONS "OPTIONS"
#define BULFIUS_STAR    "*"

// Websocket
#define BULFIUS_WEBSOCKET      "/ui"
#define BULFIUS_PREVIEW_WS     "/ui/preview"
// Endpoints
#define BULFIUS_PLUGINS        "/plugins"
#define BULFIUS_COLORMAP       "/colormap"
#define BULFIUS_IMAGE          "/image"
// Vue UI endpoints
#define BULFIUS_FAVICO_URL     "/favicon.ico"
#define BULFIUS_CSS_ROOT       "/css"
#define BULFIUS_IMG_ROOT       "/img"
#define BULFIUS_JS_ROOT        "/js"
#define BULFIUS_MEDIA_ROOT     "/media"
#define BULFIUS_DEMOS_ROOT     "/sequences"
#define BULFIUS_STATIC_FMT     ":file"
#define BULFIUS_INDEX_FILE     "index.html"
#define BULFIUS_FAVICO_ICO     "favicon.ico"
#define BULFIUS_SETTINGS       "/settings"
#define BULFIUS_FAVORITES      "/settings.html" // FIXME: the VUI should use /settings for the favorites
// REST API
#define BULFIUS_COMMANDS       "/commands"
#define BULFIUS_FRAME          "/frame"
#define BULFIUS_PARAMETERS     "/parameters"
#define BULFIUS_PARAMETERS_FMT "/:plugin"
#define BULFIUS_STATISTICS     "/statistics"
#define BULFIUS_SEQUENCE       "/sequence"
#define BULFIUS_SEQUENCES      "/sequences"
#define BULFIUS_COMMAND        "/command"
#define BULFIUS_COMMAND_FMT    "/:cmd"
// Vui pages
#define BULFIUS_INDEX  1
#define BULFIUS_FAVICO 2
#define BULFIUS_CSS    3
#define BULFIUS_IMG    4
#define BULFIUS_JS     5
#define BULFIUS_MEDIA  6
#define BULFIUS_DEMOS  7

// OPTIONS
int callback_options(const struct _u_request *, struct _u_response *, void *);

// GET
int callback_get_colormap(const struct _u_request *, struct _u_response *, void *);
int callback_get_commands(const struct _u_request *, struct _u_response *, void *);
int callback_get_image(const struct _u_request *, struct _u_response *, void *);
int callback_get_frame(const struct _u_request *, struct _u_response *, void *);
int callback_get_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_get_plugins(const struct _u_request *, struct _u_response *, void *);
int callback_get_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_get_statistics(const struct _u_request *, struct _u_response *, void *);
int callback_get_settings(const struct _u_request *, struct _u_response *, void *);
// GET/Vue UI
int callback_vui_get_static(const struct _u_request *, struct _u_response *, void *);

// POST
int callback_post_command(const struct _u_request *, struct _u_response *, void *);
int callback_post_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_post_plugins(const struct _u_request *, struct _u_response *, void *);
int callback_post_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_post_sequences(const struct _u_request *, struct _u_response *, void *);
int callback_post_settings(const struct _u_request *, struct _u_response *, void *);

// Websockets
int callback_websocket(const struct _u_request *, struct _u_response *, void *);
int callback_preview_websocket(const struct _u_request *, struct _u_response *, void *);
void bulfius_websocket_broadcast_json_message(struct Context_s *, json_t *, const struct _websocket_manager *);
void bulfius_websocket_send_command_result(struct Context_s *, const enum Command, json_t *, struct _websocket_manager *);

// Commands
int is_allowed(const enum Command cmd);
enum Command str2command(const char *);
const char *command2str(const enum Command);

// Reports
void bulfius_post_report(const char *, json_t *, json_t *);
void bulfius_send_command_result(struct Context_s *, const char *, const char *, const json_t *, const json_t *, struct _websocket_manager *);

#endif /* __BINIOU_ULFIUS_H */
