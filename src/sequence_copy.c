/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sequence.h"
#include "globals.h"


void
Sequence_copy(struct Context_s *ctx, const Sequence_t *from, Sequence_t *to)
{
  GList *tmp;

  to->id = from->id;

  if (NULL != to->name) {
    xfree(to->name);
  }

  if (NULL != from->name) {
    to->name = strdup(from->name);
  }

  tmp = g_list_first(to->layers);
  while (NULL != tmp) {
    Layer_t *l = (Layer_t *)tmp->data;

    Layer_delete(l);
    tmp = g_list_next(tmp);
  }
  g_list_free(to->layers);
  to->layers = NULL;

  tmp = g_list_first(from->layers);
  while (NULL != tmp) {
    Layer_t *lfrom = (Layer_t *)tmp->data;
    Layer_t *lto = Layer_copy(lfrom);

    to->layers = g_list_append(to->layers, (gpointer)lto);
    tmp = g_list_next(tmp);
  }

  to->lens = from->lens;

  to->image_id = from->image_id;
  to->auto_images = from->auto_images;

  to->cmap_id = from->cmap_id;
  to->auto_colormaps = from->auto_colormaps;

  if (NULL != to->params3d) {
    json_decref(to->params3d);
  }
  to->params3d = (NULL != from->params3d) ? json_deep_copy(from->params3d) : NULL;

  to->bandpass_min = from->bandpass_min;
  to->bandpass_max = from->bandpass_max;
}
