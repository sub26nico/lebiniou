/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_OPTIONS_H
#define __BINIOU_OPTIONS_H

#include "utils.h"


enum PluginOptions {
  BO_NONE = 0,
  // Sound effect
  BO_SFX = 1 << 1,
  // Graphic effect
  BO_GFX = 1 << 2,
  // Blur effect
  BO_BLUR = 1 << 3,
  // Displace effect
  BO_DISPLACE = 1 << 4,
  // Lens effect
  BO_LENS = 1 << 5,
  // Roll effect
  BO_ROLL = 1 << 6,
  // Horizontal effect
  BO_HOR = 1 << 7,
  // Vertical effect
  BO_VER = 1 << 8,
  // Changes colormap
  BO_COLORMAP = 1 << 9,
  // Splashes images
  BO_SPLASH = 1 << 10,
  // Uses images
  BO_IMAGE = 1 << 11,
  // Don't select at random
  BO_NORANDOM = 1 << 12,
  // Uses webcams
  BO_WEBCAM = 1 << 13,
  // Plugin must be alone to be cool
  BO_UNIQUE = 1 << 14,
  // Insert plugin at the begining
  BO_FIRST = 1 << 15,
  // Insert plugin at the end
  BO_LAST = 1 << 16,
  // Can be used in schemes
  BO_SCHEMES = 1 << 17,
  // Scroll effect
  BO_SCROLL = 1 << 18,
  // Mirror effect
  BO_MIRROR = 1 << 19,
  // Warp effect
  BO_WARP = 1 << 20,
  // Effect can also be used without being a lens
  BO_NOT_LENS = 1 << 21
};


#define MAX_OPTIONS 22

typedef struct PluginType_s {
  enum PluginOptions option;
  uint16_t count;
} PluginType_t;


#endif /* __BINIOU_OPTIONS_H */
