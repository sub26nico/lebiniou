/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "colormaps.h"
#include "images.h"


json_t *
vui_selector_change(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  const json_t *item_j = json_object_get(arg, "item");
  const json_t *type_j = json_object_get(arg, "type");
  const json_t *n_j = json_object_get(arg, "n");

  if (json_is_string(item_j) && json_is_string(type_j)) {
    const char *item = json_string_value(item_j);
    const char *type = json_string_value(type_j);

    if (is_equal(item, "colormap") || is_equal(item, "image")) {
#ifdef DEBUG_COMMANDS
      printf(">>> VUI_SELECTOR_CHANGE item: %s, type: %s\n", item, type);
#endif
      if (is_equal(type, "random")) {
        if (is_equal(item, "colormap")) {
          CmapFader_random(ctx->cf);
          res = CmapFader_command_result(ctx->cf);
        } else { // must be "image" then
          ImageFader_random(ctx->imgf);
          res = ImageFader_command_result(ctx->imgf);
        }
      } else if (json_is_integer(n_j)) {
        const int n = json_integer_value(n_j);
        if (is_equal(type, "previous")) {
          if (is_equal(item, "colormap")) {
            CmapFader_prev_n(ctx->cf, n);
            res = CmapFader_command_result(ctx->cf);
          } else { // must be "image" then
            ImageFader_prev_n(ctx->imgf, n);
            res = ImageFader_command_result(ctx->imgf);
          }
        } else if (is_equal(type, "next")) {
          if (is_equal(item, "colormap")) {
              CmapFader_next_n(ctx->cf, n);
              res = CmapFader_command_result(ctx->cf);
          } else { // must be "image" then
            ImageFader_next_n(ctx->imgf, n);
            res = ImageFader_command_result(ctx->imgf);
          }
        }
      }
    }
  }

  return res;
}
