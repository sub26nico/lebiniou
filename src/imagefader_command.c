/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "imagefader.h"


inline json_t *
ImageFader_command_result(const ImageFader_t *imgf)
{
  return (NULL != imgf) ? json_pack("{ss}", "image", imgf->dst->name) : NULL;
}


json_t *
ImageFader_command(ImageFader_t *imgf, const enum Command cmd)
{
  switch (cmd) {
  case CMD_IMG_PREVIOUS:
#ifdef DEBUG_COMMANDS
    printf(">>> CMD_IMG_PREVIOUS\n");
#endif
    ImageFader_prev(imgf);
    return ImageFader_command_result(imgf);

  case CMD_IMG_NEXT:
#ifdef DEBUG_COMMANDS
    printf(">>> CMD_IMG_NEXT\n");
#endif
    ImageFader_next(imgf);
    return ImageFader_command_result(imgf);

  case CMD_IMG_RANDOM:
#ifdef DEBUG_COMMANDS
    printf(">>> CMD_IMG_RANDOM\n");
#endif
    ImageFader_random(imgf);
    return ImageFader_command_result(imgf);

  default:
    printf("Unhandled image command %d\n", cmd);
    return NULL;
  }
}
