/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "colormaps.h"
#include "images.h"


json_t *
vui_shortcut(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;
  const json_t *action_j = json_object_get(arg, "action");
  const json_t *item_j = json_object_get(arg, "item");
  const json_t *index_j = json_object_get(arg, "index");

  if (json_is_string(action_j) && json_is_string(item_j) && json_is_integer(index_j)) {
    const char *action = json_string_value(action_j);
    const char *item = json_string_value(item_j);
    const int index = json_integer_value(index_j);

    if ((index >= 0) && (index < MAX_SHORTCUTS)) {
#ifdef DEBUG_COMMANDS
      printf(">>> VUI_SHORTCUT action: %s, item: %s, index: %d\n", action, item, index);
#endif
      if (is_equal(item, "colormap")) {
        if (is_equal(action, "use")) {
          const uint32_t id = ctx->shortcuts[SH_COLORMAP][index];
          if (id) {
            ctx->sm->cur->cmap_id = id;
            ctx->cf->fader->target = Colormaps_index(id);
            CmapFader_set(ctx->cf);
            res = json_pack("{ss}", "colormap", Colormaps_name(id));
          }
        } else if (is_equal(action, "store")) {
          const uint32_t id = ctx->sm->cur->cmap_id;
          ctx->shortcuts[SH_COLORMAP][index] = id;
          Context_save_shortcuts(ctx);
          res = json_pack("{sssisI}", "colormap", Colormaps_name(id), "index", index, "id", id);
        } else if (is_equal(action, "clear")) {
          ctx->shortcuts[SH_COLORMAP][index] = 0;
          Context_save_shortcuts(ctx);
          res = json_pack("{sssi}", "cleared", "colormap", "index", index);
        }
      } else if (is_equal(item, "image")) {
        if (is_equal(action, "use")) {
          const uint32_t id = ctx->shortcuts[SH_IMAGE][index];
          if (id) {
            ctx->sm->cur->image_id = id;
            ctx->imgf->fader->target = Images_index(id);
            ImageFader_set(ctx->imgf);
            res = json_pack("{ss}", "image", Images_name(id));
          }
        } else if (is_equal(action, "store")) {
          const uint32_t id = ctx->sm->cur->image_id;
          ctx->shortcuts[SH_IMAGE][index] = id;
          Context_save_shortcuts(ctx);
          res = json_pack("{sssisI}", "image", Images_name(id), "index", index, "id", id);
        } else if (is_equal(action, "clear")) {
          ctx->shortcuts[SH_IMAGE][index] = 0;
          Context_save_shortcuts(ctx);
          res = json_pack("{sssi}", "cleared", "image", "index", index);
        }
      }
    }
  }

  return res;
}
