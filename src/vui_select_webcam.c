/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
vui_select_webcam(Context_t *ctx, const json_t *arg)
{
#ifdef WITH_WEBCAM
  if ((ctx->webcams > 1) && json_is_number(arg)) {
    ctx->cam = json_integer_value(arg) % ctx->webcams;
    return json_pack("{si}", "webcam", ctx->cam);
  }
#endif

  return NULL;
}
