/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
vui_post_sequence(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  if (NULL != arg) {
    Sequence_t *seq = Sequence_from_json(arg);

    if (NULL != seq) {
      Sequence_display(seq);
      Sequence_copy(ctx, seq, ctx->sm->next);
      Context_set(ctx);
      Sequence_delete(seq);
      res = json_pack("O", arg);
    }
  }

  return res;
}
