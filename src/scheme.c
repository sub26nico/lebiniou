/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "scheme.h"


Scheme_t *
Scheme_new(const uint8_t size)
{
  Scheme_t *scheme = xcalloc(1, sizeof(Scheme_t));

  scheme->size = size;
  scheme->items = xcalloc(size, sizeof(SchemeItem_t *));
  for (uint8_t i = 0; i < size; i++) {
    scheme->items[i] = xcalloc(1, sizeof(SchemeItem_t));
  }

  return scheme;
}


void
Scheme_delete(Scheme_t *scheme)
{
  if (NULL != scheme) {
    for (uint8_t i = 0; i < scheme->size; i++) {
      xfree(scheme->items[i]);
    }
    xfree(scheme->items);
    xfree(scheme);
  }
}


enum AutoMode
Scheme_str2AutoMode(const char *str)
{
  if ((NULL == str) || is_equal(str, "random")) {
    return AM_RANDOM;
  }
  if (is_equal(str, "enable")) {
    return AM_ENABLE;
  }
  if (is_equal(str, "disable")) {
    return AM_DISABLE;
  }

  fprintf(stderr, "[!] Failed to parse AutoMode '%s'\n", str);

  return AM_RANDOM;
}


const char *
Scheme_AutoMode2str(const enum AutoMode mode)
{
  switch (mode) {
  case AM_RANDOM:
    return "random";
    break;

  case AM_ENABLE:
    return "enable";
    break;

  case AM_DISABLE:
    return "disable";
    break;
  }

  fprintf(stderr, "[!] Invalid AutoMode %d\n", mode);

  return "invalid";
}
